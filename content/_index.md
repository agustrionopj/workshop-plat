---
title: Home
# description: Welcome to this sample project
# images: ["/images/sample.jpg"]
---
This is project based workshop for undergraduate and graduate student of Astronomy ITB. There are two series of workshop that will be conducted on March 21 and August 10-11, 2020. The workshop is limited only for 15 selected participants. At the end of the first series, the participants will be assigned into several working groups. Each group will conduct different research topics which can be carried out during the period between the two workshop series. Please note that participants must attend all workshop series and bring their own laptops.

The workshop courses are:

#### First series (March 21, 2020):
- Introduction to Photographic Plate and its History (Lecture)
- Photographic Archives Digitization Program at Other Observatories (Lecture)
- Sciences of Plate Archive (Lecture)
- Plate Archives at Bosscha Observatory (Lecture)
- Preserving and Digitizing Plate Archives (Workshop)

#### Second series (August 10-11, 2020):
- Plate Scale Measurement and Astrometry of Zeiss Plates (Workshop)
- Historical Seeing Measurement using Zeiss Plates (Workshop)
- Astrometry and Photometry Measurement of Schmidt's Direct Plates (Workshop)

We are pleased to invite you to attend the workshop. You can access the registration form [here](https://forms.gle/vqwXJZKQhpTD6V4aA). The registration deadline is on March 12, 2020 at 23.59 WIB (UTC +7). The selected participants will be informed via email on March 13, 2020.

---
For more information, please contact: 

#### Irfan Imaduddin
i.imaduddin [at] students.itb.ac.id

---
<!-- 
[Get to know me better](/about "Get to know me better") -->